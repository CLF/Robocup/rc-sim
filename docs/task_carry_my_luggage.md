# Carry My Luggage
The robot should help the operator to carry a bag to a car outside.


## motivation
This rc-sim plugin delivers the environment setup and the operator interaction for the *carry my luggage task*.
The target was to develop RoboCup@Home tasks within the simulation without access to the laboratory. Gazebo provided the simulation environment that we used for the task. The task should run as far as possible without human interaction. The simulated operator and robot should communicate with each other during the task, as in real life.

## usage
Start gazebo simulation with carry my luggage plugin:
```
    catkin build
    source ${CATKIN_WS}/install/setup.sh
    roslaunch rcsim_gazebo task_carry_my_luggage.launch
```
The world name is defined in the launch file, *carry_my_luggage_**simple*** and *carry_my_luggage* are available.

## setup
* the robot's internal map of the room should be updated before the task
  * the robot explores the area outside during the task

### configuration
The communication between robot and operator is based on rostopic messages. These are string-based commands, sent via rostopic. 

The topics need to be modified inside the task_config header file to match your specific setup, the default values are:

* `/ps_adapter/custom_rec`
   * sends messages like "*we arrived*" to the robot
*  `/rcsim/operator/input`
   * receives messages like "*go ahead*" from the robot


## procedure
The robot starts in the middle of the room inside the map, the spawn point is at origin (0,0,0).


Inside the Gazebo simulation there are three buttons generated on the top left corner from the rcsim_task_core plugin.

* *"Start Task"* starts the procedure. The operator is spawned and walks to the robot.
* *"Stop Task"* stops the current task.
* *"Show Score"* shows the current store.


During the task, the robot should:

* pick up the bag
* follow the operator to the car
  * avoid objects
  * avoid crowd barriers
  * keep track of the operator
* handover the bag
* return to start position


### interaction with operator

The flow chart shows the execution of the task. The robot's messages are marked in blue, those from the operator are marked in yellow.

A natural linguistic interaction was maintained to achieve the second main goal: regain operator’s track by natural interaction

![image](cml_task.png)


#### operator receives messages from robot:

| command 	|  description	|
|-	|-	|
|  Hello human, I will carry your luggage	|  welcome signal, robot recognized operator and task begins 	|
|  i will pickup the bag	|  answer for pick up question	|
|  i cant pickup the bag	|  answer for pick up question 	|
|  I am ready to go	|   pickup complete, robot is ready for following	|
| go ahead 	| operator starts following 	|
| please wait	|  operator stops following and rotates to robot for people tracking	|
|  move slower	|  operator reduces velocity 	|
|  move faster	|  operator increases velocity 	|
|  please wave for me	|  robot lost operator, asks to wave for recovery  	|
|  here is your bag	|  bag takeover, when reached the car or final waypoint  	|

The received string comannds have to match exactly, so that they are recognized by this plugin.

#### operator sends messages to robot:

|  command	| description 	|
|-	|-	|
| can you pick up the bag 	| ask robot for pick up the bag	|
| please follow me 	| start following	|
| we arrived 	|  when car reached, stop following and start taking over the bag	|
| thanks you can go back now 	|  robot should drive to start position	|

### randomization in world
As a distraction, two other actors are loaded and intersect with the robots path as a crowd barrier.

#### random operator path
* the operator's destination route is randomly generated
  * operator waypoints are adapted along this route
  * car position is generated accordingly at the end of the waypoints
  * tiny object is placed between two waypoints

### limitations
The procedure and the communication to the operator is hard-coded. A human could act individually to problems.
The handover is not implemented from the operator's side, due to simulation restrictions. The robot can place the bag on the ground, but there is no "take bag" animation from the operator.

### problems
A few predictable problems during the following are captured, for example re-finding the operator.

#### re-finding
* The robot can control the operator's speed with *move slower* and *move faster*, additionally it can stop the operator completely with *please wait*. If the action of following is stopped, the operator rotates to the robot, to improve the tracking of people.
* *please wave for me* should be called when the robot could not find the operator. The operator switches to a waving animation, the robot can search for a waving person, the waving animation will switch back when the robot says *go ahead*


#### differences to rulebook
* points are uncountable inside a simulation
  * non-natural interaction
  * direct contact
  * no handover (from the operator)
* distance between operator and robot is tracked
  * handover: robot is near the operator
  * return phase: robot is near the start position
* automatic cancellation
  * timeout during stop
  * following: if robot lost the operator

### scoring
According to the robocup rulebook, the score which the robot could have reached in the current task is counted automatically.

![image](cml_score.png)


the score is calculated with the following conditions:

* can't find robot in world
  * abort task and show current score
* main goals
  * take the bag to the car
* bonus goals
  * reentering the arena
  * avoid the crowd obstructing path
  * avoid the small object on the ground
  * avoid the hard-to-see 3D object
  * avoid the area blocked with retractable barriers

