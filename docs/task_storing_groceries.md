# Storing Groceries
The YCB standard object set is used (models for gazebo can be found here: https://github.com/CentralLabFacilities/gazebo_ycb).
The objects on the table and in the shelf are randomly spawned, in such a way that the task is solvable.
Points are scored for each object that the robot moves from the table to the correct category in the shelf.
There are currently no tiny or heavy objects and related bonus points cannot be achieved.
No Deus ex Machina/human assistance is possible.
Please spawn your robot at (0, 0) with a yaw angle of 0.
The entrance door of the arena automatically opens when the start button is pressed.

## Setup
Start gazebo simulation with storing groceries plugin:  
```
    catkin build
    source ${CATKIN_WS}/install/setup.sh
    roslaunch rcsim_gazebo task_storing_groceries.launch
```
The world name is defined in the launch file, storing_groceries.world and storing_groceries_static.world are available. The static world is loaded via a different launch file task_storing_groceries_static.launch.

## Object categories
The objects are currently separated into four different groups: Snacks, Breakfast, Food and Drinks.
These groups are determined in the constants.h with object names provided. Other objects can be added here. 
Of these groups, having random spawning integrated, there is a chance, that single objects are placed in a separate group, e.g. 'clf_baerenmarke' can be in group Drinks or be separated.
Having these groups, the objects placed on the table have to be put into the shelf at the correct position in shelf.
With Random Spawn Integration the objects are randomly positioned on the table as well as randomly allocated into the shelf.
Like specified in the Rulebook, there are between 5 and 10 objects set on the table and just as many or even more placed in the shelf.

## Task Scoring
There is a plugin integrated to score the task, giving points for each object on table that is correctly placed in the shelf by the robot.
In contrast to the Rulebook, opening the shelf door is currently not scored, since the robot is having too much trouble doing so.
