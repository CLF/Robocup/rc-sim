# rc-sim
## What's this?
With this project, you can simulate and automatically score tasks from the RoboCup@Home competition. It currently tries to emulate the 2020 Rulebook found [here](https://github.com/RoboCupAtHome/RuleBook/pull/722) (TODO:link tag after merge?)

The simulation runs in gazebo under ROS.
Some elements in the simulation worlds are randomized like they are in the competition. The performance of the robot is automatically scored according to the rulebook.
Currently, only the tasks Carry My Luggage and Storing Groceries are supported.

Many robots are available for gazebo, for example the TIAGo (http://wiki.ros.org/Robots/TIAGo) or the Toyota HSR (https://github.com/ToyotaResearchInstitute).

TODO pictures

# Requirements

- Install `ros-melodic-desktop-full` as described [here](http://wiki.ros.org/ROS/Installation) TODO kinetic? noetic?
- [gazebo_ycb](https://github.com/CentralLabFacilities/gazebo_ycb)

# Install

```bash
# clone requirements into workspace
```
Add this project (rc-sim) and gazebo_ycb to your build server or clone and build them in a catkin workspace.

# Usage

To run the robocup tasks

1. In the world files, set ` <id_robot>robot</id_robot>` to match your robot
2. Start the simulation
```bash
# run in ROS workspace
source <workspace setup file>
roslaunch rcsim_gazebo task_XX.launch
```
3. Spawn your robot (@ 0,0,0) and start all your Robot components
4. Click `Start Task` to start the timer
5. Start the Robot behavior to solve the task

Some tasks may require dialog between the operator and the robot: check the Task Descriptions for details as the operator only understands pre defined strings.

## Task Descriptions

You can find detailed descriptions in the docs folder.

- [Carry My Luggage](docs/task_carry_my_luggage.md)
- [Storing Groceries](docs/task_storing_groceries.md)

## GUI

![Gazebo GUI](docs/gui.png)
![Score Screen](docs/score.png)
