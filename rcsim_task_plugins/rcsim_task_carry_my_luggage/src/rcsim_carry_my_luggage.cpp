#include "rcsim_task_carry_my_luggage/rcsim_carry_my_luggage.h"

#include <utility>

#include "rcsim_task_carry_my_luggage/commands.hpp"
#include "rcsim_task_carry_my_luggage/sdf_util.hpp"

void TaskCarryMyLuggage::Load(gazebo::physics::WorldPtr world, sdf::ElementPtr sdf)
{
  ROS_INFO_STREAM("TaskCarryMyLuggage Load...");
  world_ = world;

  config_ = TaskConfig::FromElement(sdf);
  task_ = new TaskSetup(*config_);

  // Init random if no seed is given
  if (config_->seed == 0)
  {
    config_->seed = std::chrono::system_clock::now().time_since_epoch().count();
  }
  rng_ = new std::mt19937(config_->seed);

  tinyObject_ = "";
  std::cout << world_->Name() << std::endl;
  if (world_->Name() == "carry_my_luggage_randomized")
  {
    CmlRandomizer randomizer(sdf, rng_, tinyObject_);
    randomizer.Randomize(world_, config_);
  } 

  ROS_INFO_STREAM("Using Config: " << *config_);

  this->node_ = gazebo::transport::NodePtr(new gazebo::transport::Node());
  this->node_->Init();
  this->pub_ = this->node_->Advertise<gazebo::msgs::Any>("~/task_score");
  this->pub_gui_msg_ = this->node_->Advertise<gazebo::msgs::Any>("~/task_message");

  this->sub_ = this->node_->Subscribe("~/rcsim", &TaskCarryMyLuggage::CbGui, this);

  this->puboperator_ = this->node_->Advertise<gazebo::msgs::Any>("~/rcsim/operator/commands");

  speech_subscriber_ = this->nh_.subscribe(config_->topic_in, 1, &TaskCarryMyLuggage::CbSpeech, this);
  speech_publisher_ = this->nh_.advertise<std_msgs::String>(config_->topic_out, 1);

  this->connections_.push_back(gazebo::event::Events::ConnectWorldUpdateBegin(
      std::bind(&TaskCarryMyLuggage::OnUpdate, this, std::placeholders::_1)));

  {
    // example add two spectators
    Spectator s1, s2;
    s1.name = "standing";
    s1.waypoints.emplace_back(9, 2, 0);

    s2.name = "walking";
    // x y v
    s2.waypoints.emplace_back(10, 1, 1);
    s2.waypoints.emplace_back(8, 1, 1);

    config_->spectators.push_back(s1);
    config_->spectators.push_back(s2);
  }
}

void TaskCarryMyLuggage::SpawnActors()
{
  ROS_DEBUG_STREAM("spawn actor");
  DisplayGuiMessage("spawn actor");
  // if (false)
  {
    // spawn operator
    std::string filename = gazebo::common::ModelDatabase::Instance()->GetModelFile("model://" + config_->id_operator);
    auto ss = std::ostringstream{};
    std::ifstream file(filename);
    ss << file.rdbuf();

    sdf::SDF sdf;
    sdf.SetFromString(ss.str());
    sdf::ElementPtr mp = sdf.Root()->GetElement("actor");
    mp->GetAttribute("name")->SetFromString(config_->id_operator);
    mp->GetElement("pose")->GetValue()->Set(config_->start);
    mp->GetElement("plugin")->InsertElement(CreateWaypointElement(config_->waypoints, config_->demo_mode));

    if (config_->demo_mode)
    {
      ROS_WARN_STREAM("RUNNING DEMO MODE, OPERATOR will move faster and start guiding");
      task_->StartIntroduction();
      StartFollowing();
    }

    world_->InsertModelSDF(sdf);
    ROS_DEBUG_STREAM("spawned operator: " << config_->id_operator);
  }

  for (auto spectator : config_->spectators)
  {
    // use first waypoint as start pose
    ignition::math::Pose3d rwstart = config_->start;
    rwstart.Pos() = spectator.waypoints.front();

    // spawn walker
    std::string filename = gazebo::common::ModelDatabase::Instance()->GetModelFile("model://random_walker");
    auto ss = std::ostringstream{};
    std::ifstream file(filename);
    ss << file.rdbuf();

    sdf::SDF sdf;
    sdf.SetFromString(ss.str());
    sdf::ElementPtr mp = sdf.Root()->GetElement("actor");
    mp->GetAttribute("name")->SetFromString(spectator.name);
    mp->GetElement("pose")->GetValue()->Set(rwstart);
    mp->GetElement("plugin")->InsertElement(CreateWaypointElement(spectator.waypoints, config_->demo_mode));
    mp->GetElement("plugin")->InsertElement(CreateStringElement("id_robot", config_->id_robot));

    world_->InsertModelSDF(sdf);
    ROS_DEBUG_STREAM("spawned random_walker: " << spectator.name);
  }
}

void TaskCarryMyLuggage::CbSpeech(const std_msgs::String::ConstPtr& msg)
{
  if (!started_)
  {
    return;
  }
  auto phase = task_->GetCurrentPhase();
  ROS_INFO_STREAM("got speech message" << msg->data << " phase" << phase);
  DisplayGuiMessage(config_->id_robot + ": " + msg->data);

  switch (commands::StringToCommand(msg->data))
  {
    case Commands::TASK_START:
      task_->StartTask();
      ROS_INFO_STREAM("recieved robot task start notification");
      break;
    case Commands::START:
      StartFollowing();
      break;
    case Commands::TAKEBAG_YES:
      task_->StartTakeBag();
      last_talk_ = ros::Time::now() - config_->talk_repeat_delay;
      SpawnBag();
      break;
    case Commands::TAKEBAG_NO:
    case Commands::TAKEBAG_DONE:
      task_->StartIntroduction();
      break;
    case Commands::STOP:
      PauseFollowing();
      break;
    case Commands::HANDOVERDROP:
      task_->StartReturn();
      SendMessageToRobot(commands::MSG_RO_RETURN);
      break;
    default:
      ROS_DEBUG_STREAM("cbspeech default");
      puboperator_->Publish(gazebo::msgs::ConvertAny(msg->data));
  }
}

void TaskCarryMyLuggage::SpawnBag()
{
  // todo
}

void TaskCarryMyLuggage::SendMessageToRobot(std::string message, ros::Duration delay)
{
  last_talk_ = ros::Time::now();
  ROS_DEBUG_STREAM("say: " << message << " in " << delay.toSec());
  std::thread(&TaskCarryMyLuggage::TalkThread, this, message, delay).detach();
}

void TaskCarryMyLuggage::DisplayGuiMessage(const std::string& message)
{
  pub_gui_msg_->Publish(gazebo::msgs::ConvertAny(message));
}

void TaskCarryMyLuggage::TalkThread(std::string message, const ros::Duration& delay)
{
  delay.sleep();
  DisplayGuiMessage(config_->id_operator + ": " + message);
  std_msgs::String msg;
  msg.data = std::move(message);
  this->speech_publisher_.publish(msg);
}

void TaskCarryMyLuggage::OnUpdate(const gazebo::common::UpdateInfo& /*info*/)
{
  if (!world_->EntityByName(config_->id_operator))
  {
    return;
  }
  if (!started_)
  {
    return;
  }
  if (task_->GetScorePtr()->GetRemainingTime() <= ros::Duration(0))
  {
    ScoreTask();
    return;
  }

  auto score = task_->Update(world_);
  pub_->Publish(gazebo::msgs::ConvertAny(score.GenerateHTMLScoreString()));

  auto phase = task_->GetCurrentPhase();
  ROS_DEBUG_STREAM_THROTTLE(5, "current phase: " << phase);

  auto operatorpose = world_->EntityByName(config_->id_operator)->WorldPose();
  auto opxy = ignition::math::Vector2d(operatorpose.Pos().X(), operatorpose.Pos().Y());

  // todo? move stuff to task setup calls?
  // should do all distance checking there
  if (phase == TaskPhase::INIT)
  {
    if (ros::Time::now() > last_talk_ + config_->talk_repeat_delay)
    {
      SendMessageToRobot(commands::MSG_RO_BAG);
    }
  }
  else if (phase == TaskPhase::INTRODUCTION)
  {
    if (ros::Time::now() > last_talk_ + config_->talk_repeat_delay)
    {
      SendMessageToRobot(commands::MSG_RO_START);
    }
  }
  else if (phase == TaskPhase::FOLLOWING)
  {
    auto target = config_->waypoints.back();
    ignition::math::Vector2d targetxy = ignition::math::Vector2d(target.X(), target.Y());
    double dist = opxy.Distance(targetxy);
    ROS_DEBUG_THROTTLE(2, "target following, x: %f, y: %f, dist: %f", target.X(), target.Y(), dist);
    bool finished = dist < 0.01;
    if (finished)
    {
      StartCarReached();
    }
  }
  else if (phase == TaskPhase::HANDOVER)
  {
    if (ros::Time::now() > last_talk_ + config_->talk_repeat_delay)
    {
      SendMessageToRobot(commands::MSG_RO_TARGET_REACHED);
    }
  }
}

void TaskCarryMyLuggage::StartCarReached()
{
  task_->StopFollowing();
  task_->StartHandover();
  last_talk_ = ros::Time::now() - config_->talk_repeat_delay;
}

void TaskCarryMyLuggage::PauseFollowing()
{
  task_->StopFollowing();
  puboperator_->Publish(gazebo::msgs::ConvertAny(commands::MSG_OP_STOP));
}

void TaskCarryMyLuggage::StartFollowing()
{
  task_->StartFollowing();
  puboperator_->Publish(gazebo::msgs::ConvertAny(commands::MSG_OP_START));
}

// void TaskStoringGroceries::CbGUI(const std::string& msg) {
void TaskCarryMyLuggage::CbGui(const boost::shared_ptr<const gazebo::msgs::Any>& msg)
{
  if (msg->string_value() == "start")
  {
    InitializeTask();
  }
  else if (msg->string_value() == "stop")
  {
    ScoreTask();
  }
}

void TaskCarryMyLuggage::InitializeTask()
{
  if ((task_ != nullptr) && !started_)
  {
    ROS_INFO_STREAM("Starting Task");

    SpawnActors();

    task_->GetScorePtr()->StartTask();

    pub_->Publish(gazebo::msgs::ConvertAny(task_->GetScorePtr()->GenerateHTMLScoreString()));
    ROS_INFO_STREAM("CURRENT SCORE:" << std::endl << task_->GetScorePtr()->GenerateScoreString());
    started_ = true;
  }
  else
  {
    ROS_INFO_STREAM("no Task or started already");
  }
}

void TaskCarryMyLuggage::ScoreTask()
{
  // ROS_INFO_STREAM("Stopping Task");
  task_->Update(world_);
  auto final_score = task_->FinishTask();
  pub_->Publish(gazebo::msgs::ConvertAny(final_score.GenerateHTMLScoreString()));
  // ROS_INFO_STREAM("FINAL SCORE:" << std::endl << final_score.GenerateScoreString());
}

// Register this plugin with the simulator
GZ_REGISTER_WORLD_PLUGIN(TaskCarryMyLuggage)
