#pragma once

#include <vector>
#include <unordered_map>
#include <list>

#include "rcsim_task_storing_groceries/model.h"
#include "rcsim_task_storing_groceries/group.h"

namespace m = ignition::math;

/**
 * Plugin used to randomize model and group instantiation.
 */
class RandomSpawn
{
private:
  gazebo::physics::WorldPtr world_;
  // Using Mersenne Twister Pseudo-number generator
  std::mt19937* rng_{ nullptr };

  // Uses model pose and space for randomized new pose
  void RandomPose(Model* model);

public:
  // Enables / Disables collision checking
  bool collision_check{ true };

  // Counts how often OnUpdate() has been called, since all physics need to be loaded for collision check
  int collision_check_counter = 200;

  RandomSpawn(gazebo::physics::WorldPtr world, std::mt19937* rng);

  // ALTERNATIVE Load Storing Groceries World and initialize models by separation
  void LoadWorld(std::list<Model> models);

  // Changes pose of model randomly and adds to gazebo world
  // Optional, fix position and rotation to exclude randomness (default)
  void AddModel(Model model);

  // Swaps pose of models randomly and add each model using AddModel()
  // optional fix position and rotation to exclude randomness (default)
  void AddGroup(std::list<Model> models);

  // Set random pose of list to each object of group before randomize each model position in AddModel, respectively
  // Returns groups with each pose
  std::list<Group> AddGroups(std::list<Group> groups, std::list<m::Pose3d> pose_list);

  // Has to be called in OnUpdate() from TaskPlugin
  // Takes all collisions from inserted models and fixes location by moving apart
  void ResolveCollisions(std::list<Model> models);

  // returns map of random objects with modelname and list of objectnames from given group list
  std::list<Model> ChooseModelsFromGroup(std::list<std::string> group, const std::string& group_name, int count,
                                         const m::Pose3d& pose = POSE_DEFAULT, const Vector4d& space = SPACE_DEFAULT);

  template <typename T>
  void ShuffleList(std::list<T>* list)
  {
    // Sample methods for iterators
    std::vector<T> v(list->begin(), list->end());
    shuffle(v.begin(), v.end(), *rng_);
    list->assign(v.begin(), v.end());
  }
};
