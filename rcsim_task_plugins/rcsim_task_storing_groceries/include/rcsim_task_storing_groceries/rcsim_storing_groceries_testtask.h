#include <gazebo/gazebo.hh>
#include <gazebo/transport/transport.hh>

#include <rcsim_task_core/task_score.h>

#include "rcsim_task_storing_groceries/task_setup.h"

class TaskStoringGroceries : public gazebo::WorldPlugin
{
public:
  void Load(gazebo::physics::WorldPtr world, sdf::ElementPtr sdf) override;

  // void CbGUI(const std::string& msg);
  void CbGUI(const boost::shared_ptr<const gazebo::msgs::Any>& msg);

  // starts the task
  void StartTask();

  // score the task
  void ScoreTask();

private:
  TaskSetup* task_{ nullptr };
  bool started_{ false };

  gazebo::transport::NodePtr node_;
  gazebo::transport::PublisherPtr pub_;
  gazebo::transport::SubscriberPtr sub_;
  gazebo::physics::WorldPtr world_;
};