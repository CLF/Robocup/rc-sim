#ifndef RCSIM_STORING_GROCERIES_GROUP_H
#define RCSIM_STORING_GROCERIES_GROUP_H

#include <list>

#include "rcsim_task_storing_groceries/model.h"

namespace m = ignition::math;

/**
 *  Contains string list of ALL model names in group,
 *  list of models currently placed in world, and the pose for the whole group.
 */
class Group
{
private:
  std::list<std::string> model_names_;
  std::list<Model> models_;
  m::Pose3d group_pose_;

public:
  Group(std::list<std::string> model_names, std::list<Model> models, const m::Pose3d& group_pose);

  std::list<std::string> GetModelNames()
  {
    return model_names_;
  }

  std::list<Model> GetModels()
  {
    return models_;
  }

  m::Pose3d* GetGroupPose()
  {
    return &group_pose_;
  }

  // Returns True iff model is in group
  bool CheckModel(Model model);

  // Returns True iff model with specified name is in group
  bool CheckModelByName(const std::string& modelname);

  // Returns True iff model with specified objectname is in group
  bool CheckModelByObjectName(const std::string& objectname);

  void SetModelNames(std::list<std::string> model_names);

  void SetModels(std::list<Model> models);

  void SetGroupPose(const m::Pose3d& group_pose);
};

#endif