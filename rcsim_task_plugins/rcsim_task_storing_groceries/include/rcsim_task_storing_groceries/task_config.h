#pragma once

#include <utility>

#include "rcsim_task_storing_groceries/task_setup.h"

struct WorldObject
{
  explicit WorldObject(const sdf::ElementPtr& object)
  {
    object->GetElement("name")->GetValue()->Get(name);
    object->GetElement("category")->GetValue()->Get(category);
    if (object->HasElement("type"))
    {
      std::string stype;
      object->GetElement("category")->GetValue()->Get(stype);
      type = StringToObjectType(stype);
    }
  }

  WorldObject(std::string pname, std::string pcategory)
  {
    name = std::move(pname);
    category = std::move(pcategory);
  }

  std::string name;
  std::string category;
  ObjectType type{ ObjectType::NORMAL };
};

struct Table
{
};

struct Shelf
{
};

struct TaskConfig
{
  static TaskConfig* FromElement(const sdf::ElementPtr& config);

  int seed{ 0 };
  std::list<WorldObject> world_objects;
  Table table;
  Shelf shelf;

  friend std::ostream& operator<<(std::ostream& out, const TaskConfig& tc);
};
