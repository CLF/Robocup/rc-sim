#pragma once

#include <unordered_map>
#include <list>
#include <string>

#include <ignition/math/Pose3.hh>
#include <ignition/math/Vector4.hh>

namespace m = ignition::math;
using Vector4d = m::Vector4<double>;

// DEFAULT QUATERNION
static const m::Quaterniond DEF_QUAT(0, -0, -0);
// QUATERNION 90° rotation
static const m::Quaterniond DEF_QUAT_ROT(0, 0, -1.5708);

// DEFAULT CLF SHELF VALUES -- to be replaced when moving to other positions
static const float CLF_SHELF_MODELS_X = 7.60;
static const float CLF_SHELF_MODELS_Y = 1.80;

static const float CLF_SHELF_MODELS_Z_ZERO = 0.416;
static const float CLF_SHELF_MODELS_Z_ONE = 0.770;
static const float CLF_SHELF_MODELS_Z_TWO = 1.050;
static const float CLF_SHELF_MODELS_Z_THREE = 1.400;

// DEFAULT CLF TABLE VALUES -- to be replaced when moving to other positions
static const float CLF_TABLE_MODELS_X = 6.14;
static const float CLF_TABLE_MODELS_Y = 2.80;
static const float CLF_TABLE_MODELS_Z = 0.730;

/**
 * Positions are used to predefine a place for models to be placed in (randomly).
 */

// DEFAULT POSITIONS
static const m::Pose3d POSE_DEFAULT = m::Pose3d(m::Vector3d(0, 0, 0), m::Quaterniond(0, -0, -0));

// DEFAULT POSITIONS : SHELF
static const m::Pose3d POSE_SHELF = m::Pose3d(m::Vector3d(2.00, 2.00, 0.00), DEF_QUAT);
static const m::Pose3d POSE_SHELF_TOP = m::Pose3d(m::Vector3d(2.00, 1.80, 0.83), DEF_QUAT);
static const m::Pose3d POSE_SHELF_TOP_L = m::Pose3d(m::Vector3d(1.80, 1.80, 0.83), DEF_QUAT);
static const m::Pose3d POSE_SHELF_TOP_R = m::Pose3d(m::Vector3d(2.20, 1.80, 0.83), DEF_QUAT);
static const m::Pose3d POSE_SHELF_CENTER = m::Pose3d(m::Vector3d(2.00, 1.80, 0.83), DEF_QUAT);
static const m::Pose3d POSE_SHELF_CENTER_L = m::Pose3d(m::Vector3d(1.80, 1.80, 0.50), DEF_QUAT);
static const m::Pose3d POSE_SHELF_CENTER_R = m::Pose3d(m::Vector3d(2.20, 1.80, 0.50), DEF_QUAT);
static const m::Pose3d POSE_SHELF_BOTTOM = m::Pose3d(m::Vector3d(2.00, 1.80, 0.10), DEF_QUAT);
static const m::Pose3d POSE_SHELF_BOTTOM_L = m::Pose3d(m::Vector3d(1.80, 1.80, 0.10), DEF_QUAT);
static const m::Pose3d POSE_SHELF_BOTTOM_R = m::Pose3d(m::Vector3d(2.20, 1.80, 0.10), DEF_QUAT);

// DEFAULT POSITIONS : CLF_SHELF
static const float MARGIN_L = +0.20;
static const float MARGIN_R = -0.20;

static const m::Pose3d POSE_CLF_SHELF =
    m::Pose3d(m::Vector3d(CLF_SHELF_MODELS_X + 0.10, CLF_SHELF_MODELS_Y, 0.000), DEF_QUAT_ROT);
static const m::Pose3d POSE_CLF_SHELF_LEVEL_ZERO =
    m::Pose3d(m::Vector3d(CLF_SHELF_MODELS_X, CLF_SHELF_MODELS_Y, CLF_SHELF_MODELS_Z_ZERO), DEF_QUAT_ROT);
static const m::Pose3d POSE_CLF_SHELF_LEVEL_ZERO_L =
    m::Pose3d(m::Vector3d(CLF_SHELF_MODELS_X, CLF_SHELF_MODELS_Y + MARGIN_L, CLF_SHELF_MODELS_Z_ZERO), DEF_QUAT_ROT);
static const m::Pose3d POSE_CLF_SHELF_LEVEL_ZERO_R =
    m::Pose3d(m::Vector3d(CLF_SHELF_MODELS_X, CLF_SHELF_MODELS_Y + MARGIN_R, CLF_SHELF_MODELS_Z_ZERO), DEF_QUAT_ROT);
static const m::Pose3d POSE_CLF_SHELF_LEVEL_ONE =
    m::Pose3d(m::Vector3d(CLF_SHELF_MODELS_X, CLF_SHELF_MODELS_Y, CLF_SHELF_MODELS_Z_ONE), DEF_QUAT_ROT);
static const m::Pose3d POSE_CLF_SHELF_LEVEL_ONE_L =
    m::Pose3d(m::Vector3d(CLF_SHELF_MODELS_X, CLF_SHELF_MODELS_Y + MARGIN_L, CLF_SHELF_MODELS_Z_ONE), DEF_QUAT_ROT);
static const m::Pose3d POSE_CLF_SHELF_LEVEL_ONE_R =
    m::Pose3d(m::Vector3d(CLF_SHELF_MODELS_X, CLF_SHELF_MODELS_Y + MARGIN_R, CLF_SHELF_MODELS_Z_ONE), DEF_QUAT_ROT);
static const m::Pose3d POSE_CLF_SHELF_LEVEL_TWO =
    m::Pose3d(m::Vector3d(CLF_SHELF_MODELS_X, CLF_SHELF_MODELS_Y, CLF_SHELF_MODELS_Z_TWO), DEF_QUAT_ROT);
static const m::Pose3d POSE_CLF_SHELF_LEVEL_TWO_L =
    m::Pose3d(m::Vector3d(CLF_SHELF_MODELS_X, CLF_SHELF_MODELS_Y + MARGIN_L, CLF_SHELF_MODELS_Z_TWO), DEF_QUAT_ROT);
static const m::Pose3d POSE_CLF_SHELF_LEVEL_TWO_R =
    m::Pose3d(m::Vector3d(CLF_SHELF_MODELS_X, CLF_SHELF_MODELS_Y + MARGIN_R, CLF_SHELF_MODELS_Z_TWO), DEF_QUAT_ROT);
static const m::Pose3d POSE_CLF_SHELF_LEVEL_THREE =
    m::Pose3d(m::Vector3d(CLF_SHELF_MODELS_X, CLF_SHELF_MODELS_Y, CLF_SHELF_MODELS_Z_THREE), DEF_QUAT_ROT);
static const m::Pose3d POSE_CLF_SHELF_LEVEL_THREE_L =
    m::Pose3d(m::Vector3d(CLF_SHELF_MODELS_X, CLF_SHELF_MODELS_Y + MARGIN_L, CLF_SHELF_MODELS_Z_THREE), DEF_QUAT_ROT);
static const m::Pose3d POSE_CLF_SHELF_LEVEL_THREE_R =
    m::Pose3d(m::Vector3d(CLF_SHELF_MODELS_X, CLF_SHELF_MODELS_Y + MARGIN_R, CLF_SHELF_MODELS_Z_THREE), DEF_QUAT_ROT);

// DEFAULT POSITIONS : CLF_TABLE_SHORT
static const m::Pose3d POSE_CLF_TABLE_SHORT =
    m::Pose3d(m::Vector3d(CLF_TABLE_MODELS_X, CLF_TABLE_MODELS_Y, 0.00), DEF_QUAT_ROT);
static const m::Pose3d POSE_CLF_TABLE_SHORT_ITEMS =
    m::Pose3d(m::Vector3d(CLF_TABLE_MODELS_X, CLF_TABLE_MODELS_Y - 0.25, CLF_TABLE_MODELS_Z), DEF_QUAT_ROT);
static const m::Pose3d POSE_CLF_TABLE_SHORT_ITEMS_L =
    m::Pose3d(m::Vector3d(CLF_TABLE_MODELS_X - 0.40, CLF_TABLE_MODELS_Y - 0.25, CLF_TABLE_MODELS_Z), DEF_QUAT_ROT);
static const m::Pose3d POSE_CLF_TABLE_SHORT_ITEMS_L_C =
    m::Pose3d(m::Vector3d(CLF_TABLE_MODELS_X - 0.20, CLF_TABLE_MODELS_Y - 0.25, CLF_TABLE_MODELS_Z), DEF_QUAT_ROT);
static const m::Pose3d POSE_CLF_TABLE_SHORT_ITEMS_C =
    m::Pose3d(m::Vector3d(CLF_TABLE_MODELS_X, CLF_TABLE_MODELS_Y - 0.25, CLF_TABLE_MODELS_Z), DEF_QUAT_ROT);
static const m::Pose3d POSE_CLF_TABLE_SHORT_ITEMS_R_C =
    m::Pose3d(m::Vector3d(CLF_TABLE_MODELS_X + 0.20, CLF_TABLE_MODELS_Y - 0.25, CLF_TABLE_MODELS_Z), DEF_QUAT_ROT);
static const m::Pose3d POSE_CLF_TABLE_SHORT_ITEMS_R =
    m::Pose3d(m::Vector3d(CLF_TABLE_MODELS_X + 0.40, CLF_TABLE_MODELS_Y - 0.25, CLF_TABLE_MODELS_Z), DEF_QUAT_ROT);

/**
 * Boundaries used to define spaces, in which a model can be spawned randomly, e.g. left and right boundaries in a
 * shelf.
 */

// DEFAULT boundaries, x_left, x_right, y_front, y_back
static const Vector4d SPACE_DEFAULT = { 0, 0, 0, 0 };
static const Vector4d SPACE_BOOKSHELF = { -0.35, 0.35, -0.30, -0.05 };
static const Vector4d SPACE_CLF_SHELF = { -0.07, 0.10, -0.20, 0.20 };

static const Vector4d SPACE_CLF_SHELF_EXTENDED = { -0.04, 0.02, -0.1, 0.1 };
// Used for table positioning in one area only
static const Vector4d SPACE_CLF_TABLE_SHORT = { -0.35, 0.45, -0.04, 0.04 };
// Used for table positioning in 5 separated areas
static const Vector4d SPACE_CLF_TABLE_SHORT_EXTENDED = { -0.15, 0.25, -0.04, 0.04 };

/**
 * Pose lists are used to have all possible poses IN a model, e.g. shelf.
 * With random spawn plugin these poses are randomly assigned to (a group of) models
 */

// DEFAULT POSE LISTS
static const std::list<m::Pose3d> POSE_LIST_SHELF({ POSE_SHELF_TOP, POSE_SHELF_CENTER, POSE_SHELF_BOTTOM });
static const std::list<m::Pose3d> POSE_LIST_SHELF_EXTENDED({ POSE_SHELF_TOP_L, POSE_SHELF_TOP_R, POSE_SHELF_CENTER_L,
                                                             POSE_SHELF_CENTER_R, POSE_SHELF_BOTTOM_L,
                                                             POSE_SHELF_BOTTOM_R });

static const std::list<m::Pose3d> POSE_LIST_CLF_SHELF({ POSE_CLF_SHELF_LEVEL_ZERO, POSE_CLF_SHELF_LEVEL_ONE,
                                                        POSE_CLF_SHELF_LEVEL_TWO, POSE_CLF_SHELF_LEVEL_THREE });

static const std::list<m::Pose3d>
    POSE_LIST_CLF_SHELF_EXTENDED({ POSE_CLF_SHELF_LEVEL_ZERO_L, POSE_CLF_SHELF_LEVEL_ZERO_R, POSE_CLF_SHELF_LEVEL_ONE_L,
                                   POSE_CLF_SHELF_LEVEL_ONE_R, POSE_CLF_SHELF_LEVEL_TWO_L, POSE_CLF_SHELF_LEVEL_TWO_R,
                                   POSE_CLF_SHELF_LEVEL_THREE_L, POSE_CLF_SHELF_LEVEL_THREE_R });


static const std::list<m::Pose3d> POSE_LIST_CLF_TABLE({ POSE_CLF_TABLE_SHORT_ITEMS_L, POSE_CLF_TABLE_SHORT_ITEMS_L_C,
                                                        POSE_CLF_TABLE_SHORT_ITEMS_C, POSE_CLF_TABLE_SHORT_ITEMS_R_C,
                                                        POSE_CLF_TABLE_SHORT_ITEMS_R });

/**
 * These group definitions are used in storing groceries task / random spawn plugin
 * to select models of a predefined group, e.g. snacks, and to identify correctly placed models for scoring later on.
 */

// GROUPS
static const std::string SNACKS = "snacks";
static const std::string BREAKFAST = "breakfast";
static const std::string FOOD = "food";
static const std::string DRINKS = "drinks";

// GROUP LISTS
static const std::list<std::string> GROUP_SNACKS = { "chips_can", "clf_pringles", "clf_apple", "cracker_box" };
static const std::list<std::string> GROUP_BREAKFAST = { "kelloggs", "smacks", "pudding_box" };
static const std::list<std::string> GROUP_FOOD = { "mustard_bottle", "sugar_box", "tuna_fish_can", "potted_meat_can",
                                                   "tomato_soup_can" };
static const std::list<std::string> GROUP_DRINKS = { "clf_baerenmarke", "bleach_cleanser" };

static const std::unordered_map<std::string, std::list<std::string>> STORING_GROCERIES_GROUPS =
    { { SNACKS, GROUP_SNACKS }, { BREAKFAST, GROUP_BREAKFAST }, { FOOD, GROUP_FOOD }, { DRINKS, GROUP_DRINKS } };
