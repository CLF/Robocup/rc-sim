#pragma once

#include <list>

#include <gazebo/gazebo.hh>
#include <sdf/sdf.hh>
#include <unordered_map>
#include <utility>

#include <rcsim_task_core/task_score.h>

enum ObjectType
{
  NORMAL,
  TINY,
  HEAVY
};

static std::unordered_map<std::string, ObjectType> const OBJECT_TYPE_MAP = { { "normal", ObjectType::NORMAL },
                                                                             { "tiny", ObjectType::TINY },
                                                                             { "heavy", ObjectType::HEAVY } };
static ObjectType StringToObjectType(const std::string& str)
{
  auto it = OBJECT_TYPE_MAP.find(str);
  if (it != OBJECT_TYPE_MAP.end())
  {
    return it->second;
  }

  return ObjectType::NORMAL;
};

struct SGObject
{
  explicit SGObject(const sdf::ElementPtr& object)
  {
    object->GetElement("name")->GetValue()->Get(name);
    object->GetElement("maxDist")->GetValue()->Get(max_target_distance);
    object->GetElement("target")->GetValue()->Get(target_pose);
  }

  SGObject(std::string pname, const ignition::math::Pose3d& ptarget_pose, double pmax_target_distance)
  {
    name = std::move(pname);
    target_pose = ptarget_pose;
    max_target_distance = pmax_target_distance;
  }

  std::string name;
  ignition::math::Pose3d target_pose;
  double max_target_distance;
  double max_height_difference = 0.2;
  ObjectType type{ ObjectType::NORMAL };
};

class TaskSetup
{
public:
  TaskSetup();
  static TaskSetup* FromElement(const sdf::ElementPtr& objects);

  TaskScore ScoreWorld(const gazebo::physics::WorldPtr& world);
  std::shared_ptr<TaskScore> GetScorePtr()
  {
    return score_;
  }

  void AddElement(const SGObject& object)
  {
    objects_.push_back(object);
  }

private:
  std::shared_ptr<TaskScore> score_;
  std::list<SGObject> objects_;
};