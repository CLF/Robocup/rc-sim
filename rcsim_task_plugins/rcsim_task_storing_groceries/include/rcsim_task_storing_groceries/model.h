#pragma once
#include <random>

#include <ignition/math/Pose3.hh>
#include <cstdlib>

#include "rcsim_task_storing_groceries/constants.h"

namespace m = ignition::math;
using Vector4d = m::Vector4<double>;

class Model
{
private:
  std::string modelname_;
  std::string objectname_;
  m::Pose3d pose_;
  Vector4d space_;

public:
  Model(std::string modelname, std::string objectname, const m::Pose3d& pose = POSE_DEFAULT,
        const Vector4d& space = SPACE_DEFAULT);

  void SetName(std::string name);
  void SetObjectName(std::string objectname);
  void SetPose(const m::Pose3d& pose);
  void SetSpace(const Vector4d& space);
  void SetRandomPose(std::mt19937* rng);

  std::string GetName()
  {
    return modelname_;
  }
  std::string GetObjectName()
  {
    return objectname_;
  }
  m::Pose3d* GetPose()
  {
    return &pose_;
  }
  Vector4d GetSpace()
  {
    return space_;
  }
};
