#pragma once

#include <ignition/math/Pose3.hh>
#include <list>
#include <random>

#include <gazebo/gazebo.hh>

#include "rcsim_task_storing_groceries/task_setup.h"
#include "rcsim_task_storing_groceries/task_config.h"
#include "rcsim_task_storing_groceries/random_spawn.h"
#include "rcsim_task_storing_groceries/group.h"
#include "rcsim_task_storing_groceries/model.h"

namespace m = ignition::math;

/**
 * Used for randomized spawn of model objects.
 */
class TaskStoringGroceries : public gazebo::WorldPlugin
{
private:
  RandomSpawn* spawner_{ nullptr };
  std::mt19937* rng_{ nullptr };

  // contains position of group in shelf
  std::list<Group> groups_;
  std::list<Model> models_;

  TaskSetup* task_{ nullptr };
  TaskConfig* config_{ nullptr };
  bool started_{ false };

  gazebo::transport::NodePtr node_;
  gazebo::transport::PublisherPtr pub_;
  gazebo::transport::SubscriberPtr sub_;
  gazebo::physics::WorldPtr world_;

  std::vector<gazebo::event::ConnectionPtr> connections_;

  sdf::ElementPtr sdf_;

  double pmaxTargetDistance_;

public:
  void Load(gazebo::physics::WorldPtr world, sdf::ElementPtr sdf) override;

  void OnUpdate(const gazebo::common::UpdateInfo& info);

  /**
   * Chooses and initializes random models in groups for shelf given in group_map
   * Returns list of different groups in shelf containing models
   */
  std::list<Group> ChooseShelfModels(std::unordered_map<std::string, int> group_map);

  /**
   * Chooses and initializes random models for table given in group_map
   * Returns list of models placed on table
   */
  std::list<Model> ChooseTableModels(std::unordered_map<std::string, int> group_map);

  /**
   *  Uses Choose...Models methods to divide and initialize models automatically
   *  Returns scoring models list
   */
  std::list<Model> AddTaskModels();

  // void CbGUI(const std::string& msg);
  void CbGUI(const boost::shared_ptr<const gazebo::msgs::Any>& msg);

  // starts the task
  void StartTask();

  // score the task
  void ScoreTask();
};
