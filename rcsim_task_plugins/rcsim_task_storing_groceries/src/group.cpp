#include <utility>

#include "rcsim_task_storing_groceries/group.h"

Group::Group(std::list<std::string> model_names, std::list<Model> models, const m::Pose3d& group_pose)
{
  this->model_names_ = std::move(model_names);
  this->models_ = std::move(models);
  this->group_pose_ = group_pose;
}

bool Group::CheckModel(Model model)
{
  for (const std::string& model_name : model_names_)
  {
    // cout << "Comparing " << model.getName() << " with " << model_name << " of group models" << endl;
    if (model.GetName() == model_name)
    {
      return true;
    }
  }
  return false;
}

bool Group::CheckModelByName(const std::string& modelname)
{
  for (const std::string& model_name : model_names_)
  {
    if (modelname == model_name)
    {
      return true;
    }
  }
  return false;
}

bool Group::CheckModelByObjectName(const std::string& objectname)
{
  for (Model model : models_)
  {
    if (objectname == model.GetObjectName())
    {
      return true;
    }
  }
  return false;
}

void Group::SetModelNames(std::list<std::string> model_names)
{
  this->model_names_ = std::move(model_names);
}

void Group::SetModels(std::list<Model> models)
{
  this->models_ = std::move(models);
}

void Group::SetGroupPose(const m::Pose3d& group_pose)
{
  this->group_pose_ = group_pose;
}