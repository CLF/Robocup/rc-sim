#include <random>
#include <utility>

#include "rcsim_task_storing_groceries/model.h"

Model::Model(std::string modelname, std::string objectname, const m::Pose3d& pose, const Vector4d& space)
{
  this->modelname_ = std::move(modelname);
  this->objectname_ = std::move(objectname);
  this->pose_ = pose;
  this->space_ = space;
}

void Model::SetName(std::string name)
{
  this->modelname_ = std::move(name);
}

void Model::SetObjectName(std::string objectname)
{
  this->objectname_ = std::move(objectname);
}

void Model::SetPose(const m::Pose3d& pose)
{
  this->pose_ = pose;
}

void Model::SetSpace(const Vector4d& space)
{
  this->space_ = space;
}
