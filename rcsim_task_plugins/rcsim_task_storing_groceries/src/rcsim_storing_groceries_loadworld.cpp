#include "rcsim_task_storing_groceries/rcsim_storing_groceries.h"

void TaskStoringGroceries::LoadWorld(list<TaskStoringGroceries::Model> models)
{
  // DEFINE TASK GROUPS HERE
  list<list<string>> TASK_GROUPS = STORING_GROCERIES_GROUPS;
  int NR_TABLE_OBJECTS = 5;

  // Add elements to group list
  list<list<TaskStoringGroceries::Model>> group_list;
  list<TaskStoringGroceries::Model> table_model_list;

  for (auto group : TASK_GROUPS)
  {
    list<TaskStoringGroceries::Model> group_models;
    for (auto model : models)
    {
      if (find(group.begin(), group.end(), model.getName()) != group.end())
      {
        model.setSpace(SPACE_CLF_SHELF);
        if (!group_models.empty() && NR_TABLE_OBJECTS != 0)
        {
          // Add to table_model_list
          // NOT RANDOM
          model.setSpace(SPACE_CLF_TABLE_SHORT);
          table_model_list.push_back(model);
          --NR_TABLE_OBJECTS;
        }
        else
        {
          model.setSpace(SPACE_CLF_SHELF);
          group_models.push_back(model);
        }
      }
    }
    if (!group_models.empty())
    {
      group_list.push_front(group_models);
    }
  }

  // /* TEST TABLE AND SHELF GROUP DIFFERENTIATION
  for (TaskStoringGroceries::Model model : table_model_list)
  {
    cout << "table group: " << model.getObjectName() << endl;
  }
  for (list<TaskStoringGroceries::Model> group : group_list)
  {
    for (auto model : group)
    {
      cout << "shelf group: " << model.getObjectName() << endl;
    }
  }
  // */

  TaskStoringGroceries::AddGroup(table_model_list, 20, false, false);
  TaskStoringGroceries::AddGroups(group_list, POSE_LIST_CLF_SHELF, 20, false, false);

  // Add single models...
  for (auto model : models)
  {
    bool inGroup = false;
    for (auto group : TASK_GROUPS)
    {
      if (find(group.begin(), group.end(), model.getName()) != group.end())
      {
        inGroup = true;
      }
    }
    if (!inGroup)
    {
      TaskStoringGroceries::AddModel(model);
    }
  }
}
