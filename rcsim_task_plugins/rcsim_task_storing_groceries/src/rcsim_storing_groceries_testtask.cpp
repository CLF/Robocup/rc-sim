#include <ignition/math/Pose3.hh>
#include "gazebo/physics/physics.hh"
#include "gazebo/common/common.hh"
#include "gazebo/gazebo.hh"

#include "sdf/sdf.hh"

#include <iostream>

#include "rcsim_task_storing_groceries/rcsim_storing_groceries_testtask.h"

void TaskStoringGroceries::Load(gazebo::physics::WorldPtr world, sdf::ElementPtr sdf)
{
  std::cout << "TaskStoringGroceries (TEST) Load..." << std::endl;
  std::cout << "Model Database URI: " << gazebo::common::ModelDatabase::Instance()->GetURI() << std::endl;

  world_ = world;

  // create a world setup
  // real plugin call here

  // in this test we get the setup from sdf
  sdf::ElementPtr elem = sdf->GetFirstElement();
  while (elem)
  {
    if (elem->GetName() == "objects")
    {
      task_ = TaskSetup::FromElement(elem);
      break;
    }
    elem = elem->GetNextElement();
  }

  this->node_ = gazebo::transport::NodePtr(new gazebo::transport::Node());
  this->node_->Init();
  this->pub_ = this->node_->Advertise<gazebo::msgs::Any>("~/task_score");
  this->sub_ = this->node_->Subscribe("~/rcsim", &TaskStoringGroceries::CbGUI, this);

  // this->connections.push_back(event::Events::ConnectPreRender(std::bind(&SystemGUI::Update, this)));
}

// void TaskStoringGroceries::CbGUI(const std::string& msg) {
void TaskStoringGroceries::CbGUI(const boost::shared_ptr<const gazebo::msgs::Any>& msg)
{
  if (msg->string_value() == "start")
  {
    StartTask();
  }
  else if (msg->string_value() == "stop")
  {
    ScoreTask();
  }
}

void TaskStoringGroceries::StartTask()
{
  if ((task_ != nullptr) && !started_)
  {
    ROS_INFO_STREAM("Starting Task");
    task_->GetScorePtr()->StartTask();
    started_ = true;
    pub_->Publish(gazebo::msgs::ConvertAny(task_->GetScorePtr()->GenerateHTMLScoreString()));
    ROS_INFO_STREAM("CURRENT SCORE:" << std::endl << task_->GetScorePtr()->GenerateScoreString());
    world_->ModelByName("door")->GetJoint("door_joint")->SetVelocity(0, -0.5);  // Open door
  }
  else
  {
    ROS_INFO_STREAM("no Task or started already");
  }
}

void TaskStoringGroceries::ScoreTask()
{
  ROS_INFO_STREAM("Stopping Task");
  auto final_score = task_->ScoreWorld(world_);
  task_->GetScorePtr()->FinishTask();
  pub_->Publish(gazebo::msgs::ConvertAny(final_score.GenerateScoreString()));
}

// Register this plugin with the simulator
GZ_REGISTER_WORLD_PLUGIN(TaskStoringGroceries)
