#include <ignition/math/Pose3.hh>
#include <utility>
#include "gazebo/physics/physics.hh"
#include "gazebo/common/common.hh"
#include "gazebo/gazebo.hh"

#include "sdf/sdf.hh"

#include <ros/ros.h>

#include "rcsim_task_storing_groceries/random_spawn.h"

RandomSpawn::RandomSpawn(gazebo::physics::WorldPtr world, std::mt19937* rng)
{
  world_ = std::move(world);
  rng_ = rng;
}

void RandomSpawn::AddModel(Model model)
{
  RandomPose(&model);
  std::cout << std::endl
            << "Model: " << model.GetName() << ", Objectname: " << model.GetObjectName()
            << ", Pose: " << *(model.GetPose()) << std::endl;

  std::string filename = gazebo::common::ModelDatabase::Instance()->GetModelFile("model://" + model.GetName());
  auto ss = std::ostringstream{};
  std::ifstream file(filename);
  ss << file.rdbuf();

  sdf::SDF sdf;
  sdf.SetFromString(ss.str());
  sdf::ElementPtr mp = sdf.Root()->GetElement("model");
  mp->GetAttribute("name")->SetFromString(model.GetObjectName());
  mp->GetElement("pose")->GetValue()->Set(*(model.GetPose()));

  world_->InsertModelSDF(sdf);
}

void RandomSpawn::RandomPose(Model* model)
{
  Vector4d space = model->GetSpace();
  double x_l = space[0];
  double x_r = space[1];
  double y_f = space[2];
  double y_b = space[3];

  if (x_l > x_r)
  {
    std::swap(x_l, x_r);
  }
  if (y_f > y_b)
  {
    std::swap(y_f, y_b);
  }

  double x_mean = (x_l + x_r) / 2;
  double y_mean = (y_f + y_b) / 2;

  double shrink_val = 1;

  // standard derivation <= |mean - x_l|/2 would guarantee 95%, sd <= |mean - x_l|/3 would even guarantee prob of 99.7%
  // in [x_l, x_r]
  double x_sd = shrink_val * (std::abs(x_mean - x_l));
  double y_sd = shrink_val * (std::abs(y_mean - y_f));

  /**
  cout << "x_l: " << x_l << ", x_r: " << x_r << ", y_f: " << y_f << ", y_b: " << y_b << endl;
  cout << "x mean: " << x_mean << ", y mean: " << y_mean << endl;
  cout << "x standard derivation: " << x_sd << ", y standard derivation: " << y_sd << endl;
  */

  std::normal_distribution<double> dis_x(x_mean, x_sd);
  std::normal_distribution<double> dis_y(y_mean, y_sd);

  double x_rand = dis_x(*rng_);
  double y_rand = dis_y(*rng_);
  // Rare case, if models pose not in space
  while (x_rand < x_l || x_rand > x_r)
  {
    x_rand = dis_x(*rng_);
  }
  while (y_rand < y_f || y_rand > y_b)
  {
    y_rand = dis_y(*rng_);
  }
  // std::cout << "random pos: x = pos.X() + " << x_rand << ", y = pos.Y() + " << y_rand << std::endl;

  m::Vector3d model_pos = model->GetPose()->Pos();
  m::Quaterniond model_rot = model->GetPose()->Rot();
  m::Vector3d rnd_pos(model_pos.X() + x_rand, model_pos.Y() + y_rand, model_pos.Z());
  m::Quaterniond rnd_rot(model_rot.X(), model_rot.Y(), rand() % 5);  // Not all values captured...
  m::Pose3d model_pose(rnd_pos, rnd_rot);
  model->SetPose(model_pose);
  // cout << "random z rotation: " << m.getPose()->Rot() << endl;
}

void RandomSpawn::AddGroup(std::list<Model> models)
{
  std::list<m::Pose3d> poses;
  for (auto& model : models)
  {
    poses.push_front(*(model.GetPose()));
  }
  ShuffleList(&poses);
  // for (auto &pose : poses) {std::cout << "Pose: " << pose << " " << std::endl;}

  // assign shuffled values to models
  auto pi = poses.begin();
  auto it = models.begin();
  for (int i = 0; i < models.size(); ++i)
  {
    auto model = next(it, i);
    // std::cout << "AddGroup - Model pose " << *(model->GetPose()) << std::endl;
    model->SetPose(*pi);
    advance(pi, 1);
    AddModel(*model);
  }
}

std::list<Group> RandomSpawn::AddGroups(std::list<Group> groups, std::list<m::Pose3d> pose_list)
{
  std::list<Group> group_list;

  std::list<int> random_index_list;
  for (int i = 0; i < pose_list.size(); i++)
  {
    random_index_list.push_back(i);
  }
  ShuffleList(&random_index_list);
  auto random_index = random_index_list.begin();
  /*
  ShuffleList(&pose_list);
  for (auto &pose : pose_list) {std::cout << "RANDOM Pose: " << pose << " " << std::endl;}
  */

  // TODO(lruegeme): pose list < groups
  if (pose_list.size() < groups.size())
  {
    // poses have to be set more than once, which is not intended
    std::cout << "RandomSpawn - AddGroups: pose_list < groups size." << std::endl;
  }

  auto git = groups.begin();

  // loop over all groups
  for (int i = 0; i < groups.size(); ++i)
  {
    Group group = *git;
    std::list<Model> models = group.GetModels();
    auto model_it = models.begin();
    auto random_pose = next(pose_list.begin(), *random_index);
    group.SetGroupPose(*random_pose);
    group_list.push_back(group);

    // loop over all models in a group
    for (int j = 0; j < models.size(); ++j)
    {
      auto model = next(model_it, j);
      std::cout << "group: " << std::to_string(i) << ", pose: " << group.GetGroupPose()->Pos() << std::endl;
      model->SetPose(*(group.GetGroupPose()));
      AddModel(*model);
    }
    advance(random_index, 1);
    advance(git, 1);
  }
  return group_list;
}

// Alternatively, use std::list<Model> models and check if model with objectname in list --> Could solve only groups
// issue
void RandomSpawn::ResolveCollisions(std::list<Model> models)
{
  if (collision_check_counter > 0)
  {
    collision_check_counter--;
  }
  bool collisions = false;
  ROS_DEBUG_STREAM_NAMED("collision", "Resolving collisions counter: " << collision_check_counter);
  const auto& contacts = world_->Physics()->GetContactManager()->GetContacts();
  for(int i = 0; i < world_->Physics()->GetContactManager()->GetContactCount(); i++)
  {
    std::string collider1 = contacts[i]->collision1->GetModel()->GetName();
    std::string collider2 = contacts[i]->collision2->GetModel()->GetName();
    bool coll1 = false, coll2 = false;
    for (Model model : models)
    {
      if (collider1 == model.GetObjectName())
      {
        coll1 = true;
      }
      else if (collider2 == model.GetObjectName())
      {
        coll2 = true;
      }
      if (coll1 && coll2)
      {
        break;
      }
    }
    if (coll1 && coll2)
    {
      collisions = true;
      auto model1 = world_->ModelByName(collider1);
      auto model2 = world_->ModelByName(collider2);
      m::Pose3d model1_pose = model1->WorldPose();
      m::Pose3d model2_pose = model2->WorldPose();

      // std::cout << "Collider 1: " << collider1 << " pose before: " << model1->WorldPose() << std::endl;
      // std::cout << "Collider 2: " << collider2 << " pose before: " << model2->WorldPose() << std::endl;

      // Collision model 1 coordinates
      float x1 = model1_pose.Pos().X();
      float y1 = model1_pose.Pos().Y();
      float z1 = model1_pose.Pos().Z();

      // Collision model 2 coordinates
      float x2 = model2_pose.Pos().X();
      float y2 = model2_pose.Pos().Y();
      float z2 = model2_pose.Pos().Z();

      const float MARGIN = 0.01;
      // To avoid adding and substracting continuing constantly
      std::uniform_real_distribution<float> dist(0.00, 0.02);
      const float RND =  dist(*rng_);
      const float RND_MARGIN = MARGIN + RND;

      if (x1 < x2)
      {
        x1 -= RND_MARGIN;
        x2 += RND_MARGIN;
      }
      else
      {
        x1 += RND_MARGIN;
        x2 -= RND_MARGIN;
      }
      if (y1 < y2)
      {
        y1 -= RND_MARGIN;
        y2 += RND_MARGIN;
      }
      else
      {
        y1 += RND_MARGIN;
        y2 -= RND_MARGIN;
      }

      m::Vector3d pos1(x1, y1, z1);
      m::Vector3d pos2(x2, y2, z2);
      m::Pose3d pose1(pos1, model1_pose.Rot());
      m::Pose3d pose2(pos2, model2_pose.Rot());
      model1->SetWorldPose(pose1);
      model2->SetWorldPose(pose2);
      // std::cout << "Collider 1: " << collider1 << " pose after: " << model1->WorldPose() << std::endl;
      // std::cout << "Collider 2: " << collider2 << " pose after: " << model2->WorldPose() << std::endl;
    }
  }
  if (!collisions && collision_check_counter == 0)
  {
    collision_check = false;
    // Used for collision check
    world_->Physics()->GetContactManager()->SetNeverDropContacts(false);
  }
}

std::list<Model> RandomSpawn::ChooseModelsFromGroup(std::list<std::string> group, const std::string& group_name,
                                                    int count, const m::Pose3d& pose, const Vector4d& space)
{
  std::list<Model> models;
  std::list<int> random_index_list;
  for (int i = 0; i < group.size(); i++)
  {
    random_index_list.push_back(i);
  }
  ShuffleList(&random_index_list);
  auto random_index = random_index_list.begin();
  auto git = group.begin();
  for (int i = 0; i < count; ++i)
  {
    // Take modelname from shuffled group list
    auto nx = std::next(git, (*random_index + i) % group.size());
    std::string objectname = group_name + "-" + std::to_string(i) + "_" + *nx;

    // std::cout << "Choose Model " << *nx << " with name " << objectname << std::endl;

    Model* model = new Model(*nx, objectname, pose, space);
    models.push_back(*model);
  }
  return models;
}
