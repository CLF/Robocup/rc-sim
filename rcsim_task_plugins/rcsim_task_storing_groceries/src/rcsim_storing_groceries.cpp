#include "gazebo/physics/physics.hh"
#include "gazebo/common/common.hh"
#include "gazebo/gazebo.hh"

#include "sdf/sdf.hh"
#include <cstdlib>
#include <cmath> /* round */

#include <iostream>
#include <chrono> /* system time */
#include <algorithm>

#include "rcsim_task_storing_groceries/rcsim_storing_groceries.h"
#include "rcsim_task_storing_groceries/constants.h"

namespace m = ignition::math;

std::list<Group> TaskStoringGroceries::ChooseShelfModels(std::unordered_map<std::string, int> group_map)
{
  std::list<Group> clf_shelf_models;
  for (auto group : group_map)
  {
    // string items in group (e.g. snacks : { "chips_can", "clf_pringles", "clf_apple", "cracker_box" })
    std::list<std::string> group_list = STORING_GROCERIES_GROUPS.at(group.first);

    // Random probability to put one item separately in a group
    int prob = 15;

    double prob_value = ((*rng_)() % (50 + prob)) / 100.0;
    // cout << "Separate item probability value for group " << group.first << ": " << prob_value << endl;
    bool item_separately = round(prob_value) != 0.0;  // Either 0 or 1
    if (item_separately)
    {
      int item_index = (*rng_)() % group_list.size();
      auto it = group_list.begin();
      std::advance(it, item_index);
      std::cout << "Put " << *it << " from group " << group.first << " into separate group." << std::endl;
      std::string objectname = "shelf_single-item_" + *it;

      Model* model = new Model(*it, objectname, POSE_CLF_SHELF_LEVEL_ONE, SPACE_CLF_SHELF_EXTENDED);
      Group single_item_group({ *it }, { *model }, POSE_DEFAULT);
      clf_shelf_models.push_back(single_item_group);
      group.second--;
      group_list.erase(it);
    }

    std::list<Model> group_models = spawner_->ChooseModelsFromGroup(
        group_list, "shelf_" + group.first, group.second, POSE_CLF_SHELF_LEVEL_ZERO, SPACE_CLF_SHELF_EXTENDED);

    Group shelf_group(group_list, group_models, POSE_DEFAULT);
    clf_shelf_models.push_back(shelf_group);
  }
  return clf_shelf_models;
}

std::list<Model> TaskStoringGroceries::ChooseTableModels(std::unordered_map<std::string, int> group_map)
{
  std::list<Model> clf_table_models;
  int group_index = 0;
  auto table_pose_list = POSE_LIST_CLF_TABLE.begin();
  for (auto group : group_map)
  {
    const std::list<std::string>& group_list(STORING_GROCERIES_GROUPS.at(group.first));
    std::string group_name = "table_" + group.first;
    auto model_poses = std::next(table_pose_list, (group_index % POSE_LIST_CLF_TABLE.size()));
    std::list<Model> group_models = spawner_->ChooseModelsFromGroup(group_list, group_name, group.second,
                                                                    *model_poses, SPACE_CLF_TABLE_SHORT_EXTENDED);
    clf_table_models.insert(clf_table_models.begin(), group_models.begin(), group_models.end());
    group_index++;
  }
  return clf_table_models;
}

std::list<Model> TaskStoringGroceries::AddTaskModels()
{
  std::list<Model> scoring_models;

  // Random amount of models to put on table
  int min_table_models = 5;
  int max_table_models = 10;
  // Random amount of objects on table, between 5 and 10
  std::uniform_int_distribution<int> distribution(min_table_models, max_table_models);
  int rnd_table_models = distribution(*rng_);

  std::unordered_map<std::string, int> storing_groceries_shelf_models;
  std::unordered_map<std::string, int> storing_groceries_table_models;
  // Initialize map with storing groceries groups with 0 object count ...
  std::list<std::string> group_names;
  for (std::pair<std::string, std::list<std::string>> group : STORING_GROCERIES_GROUPS)
  {
    group_names.push_back(group.first);
    storing_groceries_shelf_models.insert({ group.first, 0 });
    storing_groceries_table_models.insert({ group.first, 0 });
  }

  // begin with random index of storing groceries groups
  int rnd_group_idx;
  float rnd_shelf_additional;
  auto git = group_names.begin();
  for (int i = 0; i < rnd_table_models; i++)
  {
    rnd_group_idx = (*rng_)() % STORING_GROCERIES_GROUPS.size();
    auto rnd_group = next(git, rnd_group_idx);

    // Increment map of given, randomly chosen, group
    storing_groceries_table_models[*rnd_group]++;

    storing_groceries_shelf_models[*rnd_group]++;
    // Prob. of 30% to add more to shelf group
    std::uniform_int_distribution<int> distribution(0, 80);
    rnd_shelf_additional = distribution(*rng_) / 100.0;
    if (rnd_shelf_additional > 0.5)
    {
      storing_groceries_shelf_models[*rnd_group]++;
    }
  }

  for (std::pair<std::string, int> group : storing_groceries_table_models)
  {
    std::cout << "Group " << group.first << " set to " << group.second << " models." << std::endl;
  }

  std::list<Group> clf_shelf_models = ChooseShelfModels(storing_groceries_shelf_models);
  groups_ = spawner_->AddGroups(clf_shelf_models, POSE_LIST_CLF_SHELF_EXTENDED);

  std::list<Model> clf_table_models = ChooseTableModels(storing_groceries_table_models);
  spawner_->AddGroup(clf_table_models);

  // Add table models to scoring_models
  for (const Model& model : clf_table_models)
  {
    scoring_models.push_back(model);
  }
  models_.insert(models_.end(), clf_table_models.begin(), clf_table_models.end());
  for (Group group : groups_)
  {
    std::list<Model> group_models = group.GetModels();
    models_.insert(models_.end(), group_models.begin(), group_models.end());
  }

  return scoring_models;
}

void TaskStoringGroceries::Load(gazebo::physics::WorldPtr world, sdf::ElementPtr sdf)
{
  ROS_INFO_STREAM("TaskStoringGroceries Load...");
  world_ = world;

  // Used for collision check
  world_->Physics()->GetContactManager()->SetNeverDropContacts(true);

  // Initialize model list
  task_ = new TaskSetup();
  std::list<Model> world_models;

  config_ = TaskConfig::FromElement(sdf);

  // Init random if no seed is given
  if (config_->seed == 0)
  {
    config_->seed = std::chrono::system_clock::now().time_since_epoch().count();
  }

  ROS_INFO_STREAM("Using Config: " << *config_);

  ROS_INFO_STREAM("Using RandomSpawn Plugin...");
  rng_ = new std::mt19937(config_->seed);
  spawner_ = new RandomSpawn(world_, rng_);

  for (const auto& model : world_models)
  {
    spawner_->AddModel(model);
  }

  // TaskStoringGroceries::LoadWorld(models);

  // std::list of all models to be scored
  std::list<Model> scoring_models = AddTaskModels();

  // Scoring Plugin
  this->node_ = gazebo::transport::NodePtr(new gazebo::transport::Node());
  this->node_->Init();
  this->pub_ = this->node_->Advertise<gazebo::msgs::Any>("~/task_score");
  this->sub_ = this->node_->Subscribe("~/rcsim", &TaskStoringGroceries::CbGUI, this);

  this->connections_.push_back(gazebo::event::Events::ConnectWorldUpdateBegin(
      std::bind(&TaskStoringGroceries::OnUpdate, this, std::placeholders::_1)));

  double pmax_target_distance;
  for (Model& model : scoring_models)
  {
    m::Pose3d shelf_pose;
    for (Group group : groups_)
    {
      if (group.CheckModel(model))
      {
        shelf_pose = *(group.GetGroupPose());
        pmax_target_distance = 0.2 + (0.02 * group.GetModels().size());
        std::cout << model.GetName() << " pose: " << model.GetPose()->Pos() << ", shelf pose: " << shelf_pose.Pos()
                  << ", max distance to target group: " << pmax_target_distance << std::endl;
        SGObject sgo(model.GetObjectName(), shelf_pose, pmax_target_distance);
        task_->AddElement(sgo);
        break;
      }
    }
  }
}

// void TaskStoringGroceries::CbGUI(const std::string& msg) {
void TaskStoringGroceries::CbGUI(const boost::shared_ptr<const gazebo::msgs::Any>& msg)
{
  if (msg->string_value() == "start")
  {
    StartTask();
  }
  else if (msg->string_value() == "stop")
  {
    ScoreTask();
  }
}

void TaskStoringGroceries::StartTask()
{
  if ((task_ != nullptr) && !started_)
  {
    ROS_INFO_STREAM("Starting Task");
    task_->GetScorePtr()->StartTask();
    started_ = true;
    pub_->Publish(gazebo::msgs::ConvertAny(task_->GetScorePtr()->GenerateHTMLScoreString()));
    ROS_INFO_STREAM("CURRENT SCORE:" << std::endl << task_->GetScorePtr()->GenerateScoreString());
    world_->ModelByName("door")->GetJoint("door_joint")->SetVelocity(0, -0.5);  // Open door
  }
  else
  {
    ROS_INFO_STREAM("no Task or started already");
  }
}

void TaskStoringGroceries::ScoreTask()
{
  ROS_INFO_STREAM("Stopping Task");
  auto final_score = task_->ScoreWorld(world_);
  task_->GetScorePtr()->FinishTask();
  pub_->Publish(gazebo::msgs::ConvertAny(final_score.GenerateHTMLScoreString()));
  ROS_INFO_STREAM("Final SCORE:" << std::endl << final_score.GenerateScoreString());
}

void TaskStoringGroceries::OnUpdate(const gazebo::common::UpdateInfo& /*info*/)
{
  if (task_->GetScorePtr()->IsStarted() && task_->GetScorePtr()->GetRemainingTime() <= ros::Duration(0))
  {
    ScoreTask();
  }
  else
  {
    pub_->Publish(gazebo::msgs::ConvertAny(task_->GetScorePtr()->GenerateHTMLScoreString()));
  }

  if (spawner_->collision_check)
  {
    ROS_DEBUG_STREAM("Resolving collisions...");
    spawner_->ResolveCollisions(models_);
  }
}

// Register this plugin with the simulator
GZ_REGISTER_WORLD_PLUGIN(TaskStoringGroceries)
