#include "rcsim_task_storing_groceries/task_setup.h"

#include "gazebo/physics/physics.hh"

TaskSetup::TaskSetup()
{
  score_ = std::make_shared<TaskScore>("Storing Groceries", ros::Duration(300));

  // Main goal
  score_->AddItem("obj", "Move an object next to their peers in the shelf", 500);

  ScoreItem ppoint("Receiving human help (point at target location)", -150);
  ppoint.is_penalty = true;
  score_->AddItem("ppoint", ppoint);

  ScoreItem pmove("Receiving human help (move object)", -500);
  pmove.is_penalty = true;
  score_->AddItem("ppoint", pmove);

  // Bonus Goal
  score_->AddItem("bdoor", "Opening the shelf door without human help", 300);
  score_->AddItem("btiny", "Moving a tiny object", 100);
  score_->AddItem("bheavy", "Moving a heavy object", 100);
}

TaskSetup* TaskSetup::FromElement(const sdf::ElementPtr& objects)
{
  auto* task = new TaskSetup();

  sdf::ElementPtr elem = objects->GetFirstElement();
  while (elem)
  {
    if (elem->GetName() == "object")
    {
      task->objects_.emplace_back(elem);
    }
    elem = elem->GetNextElement();
  }
  // reset max obj points
  // task->score_->AddItem("obj", "Move an object next to their peers in the shelf", task->objects_.size() * 100);

  return task;
}

TaskScore TaskSetup::ScoreWorld(const gazebo::physics::WorldPtr& world)
{
  // Check if moveable objects are placed correctly
  for (auto obj : objects_)
  {
    ROS_INFO_STREAM("Checking Object: " << obj.name);
    for (const auto& model : world->Models())
    {
      if (model->GetName() == obj.name)
      {
        ROS_DEBUG_STREAM("Found Object: " << obj.name << " in world @ " << model->WorldPose().Pos());
        // Found matching model
        ignition::math::Vector2d modelxy =
            ignition::math::Vector2d(model->WorldPose().Pos().X(), model->WorldPose().Pos().Y());
        ignition::math::Vector2d targetxy =
            ignition::math::Vector2d(obj.target_pose.Pos().X(), obj.target_pose.Pos().Y());
        double dist = modelxy.Distance(targetxy);
        double hdif = abs(model->WorldPose().Pos().Z() - obj.target_pose.Pos().Z());
        ROS_DEBUG_STREAM("xydistance to target @ : "
                         << obj.target_pose.Pos() << " is: " << dist << " (max" << obj.max_target_distance << ")"
                         << " hdiff is: " << hdif << "(max" << obj.max_height_difference << ")");
        if (dist <= obj.max_target_distance && hdif <= obj.max_target_distance)
        {
          ROS_DEBUG_STREAM("adding points");
          score_->AddPoints("obj", 100);
          if (obj.type == ObjectType::TINY)
          {
            score_->SetPoints("btiny", 100);
          }
          if (obj.type == ObjectType::HEAVY)
          {
            score_->SetPoints("bheavy", 100);
          }
        }
      }
    }
  }

  // Check for help
  // todo add penalty on help requests

  // Check for door opened
  // todo check this during setup

  // Check if static objects moved somewhere (e.g dropped to the floor)
  // Does not give penalty as of 2020 Rulebook

  return *score_.get();
}
