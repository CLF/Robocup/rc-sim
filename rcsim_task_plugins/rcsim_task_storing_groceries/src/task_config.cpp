
#include "rcsim_task_storing_groceries/task_config.h"

TaskConfig* TaskConfig::FromElement(const sdf::ElementPtr& config)
{
  auto* task = new TaskConfig();

  sdf::ElementPtr elem = config->GetFirstElement();
  while (elem)
  {
    if (elem->GetName() == "objects")
    {
      sdf::ElementPtr object = elem->GetFirstElement();
      while (object)
      {
        WorldObject wo(elem);
        task->world_objects.push_back(wo);
        object = object->GetNextElement();
      }
    }
    else if (elem->GetName() == "seed")
    {
      elem->GetValue()->Get(task->seed);
    }
    else if (elem->GetName() == "table")
    {
    }
    else if (elem->GetName() == "shelf")
    {
    }
    else
    {
    }

    elem = elem->GetNextElement();
  }
  return task;
}

std::ostream& operator<<(std::ostream& out, const TaskConfig& tc)
{
  out << "seed:" << tc.seed << " WOs:";

  return out;
}