//#include "rcsim_task_core/rcsim_gui.h"
#include "rcsim_task_core/moc_rcsim_gui.cpp"

#include <sstream>
#include <gazebo/msgs/msgs.hh>

#include <QMessageBox>
#include <QScrollBar>

#include <ros/ros.h>

namespace gazebo
{
/////////////////////////////////////////////////
RCSimGUI::RCSimGUI()
{
  this->setStyleSheet("QFrame { background-color : rgba(100, 100, 100, 255); color : white;}");
  // this->setAttribute(Qt::WA_TranslucentBackground, true);
  // this->setAttribute(Qt::WA_NoSystemBackground, true );
  // this->setAttribute(Qt::WA_OpaquePaintEvent, false );

  auto* main_layout = new QVBoxLayout;

  auto* layout = new QHBoxLayout();
  layout->setContentsMargins(0, 0, 0, 0);
  auto* row = new QWidget();
  row->setLayout(layout);

  // add buttons and timer
  start_button_ = new QPushButton(tr("Start Task"));
  start_button_->setEnabled(true);
  layout->addWidget(start_button_);

  stop_button_ = new QPushButton(tr("End Task"));
  stop_button_->setEnabled(false);
  layout->addWidget(stop_button_);

  show_score_button_ = new QPushButton(tr("Show Score"));
  show_score_button_->setEnabled(false);
  layout->addWidget(show_score_button_);

  time_label_ = new QLabel(tr("TIME "));
  layout->addWidget(time_label_);

  // Add the frame to the main layout
  main_layout->addWidget(row);

  // ================================

  // create new ROW
  layout = new QHBoxLayout();
  layout->setContentsMargins(0, 0, 0, 0);
  row = new QWidget();
  row->setLayout(layout);

  // hide scroll bar and add the same bar to the left
  text_box_ = new QTextEdit();
  text_box_->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
  text_box_->viewport()->setAutoFillBackground(false);
  text_box_->setReadOnly(true);
  auto scroll_bar = text_box_->verticalScrollBar();
  layout->addWidget(scroll_bar);

  layout->addWidget(text_box_);

  // Add the frame to the main layout
  main_layout->addWidget(row);

  // auto to_bottom = new QSpacerItem(0,1000, QSizePolicy::Expanding, QSizePolicy::Expanding);
  // main_layout->addSpacerItem(to_bottom);

  // Remove margins to reduce space
  layout->setContentsMargins(0, 0, 0, 0);
  main_layout->setContentsMargins(0, 0, 0, 0);

  this->setLayout(main_layout);

  // parent is not set here but we could implement load funktion
  // but we are not hooked on resize so we can not fill the window anyway
  // ROS_INFO_STREAM("s" << this->parentWidget()->size().height());

  // Position and resize this widget
  this->move(10, 10);
  this->resize(700, 220);

  connect(start_button_, SIGNAL(clicked()), this, SLOT(OnStartButton()));
  connect(stop_button_, SIGNAL(clicked()), this, SLOT(OnStopButton()));
  connect(show_score_button_, SIGNAL(clicked()), this, SLOT(OnShowScoreButton()));

  // Create a node for transportation
  this->node_ = transport::NodePtr(new transport::Node());
  this->node_->Init();
  this->pub_ = this->node_->Advertise<msgs::Any>("~/rcsim");

  this->sub_ = this->node_->Subscribe("~/task_score", &RCSimGUI::CbTask, this);
  this->sub_msg_ = this->node_->Subscribe("~/task_message", &RCSimGUI::CbMsg, this);
}

/////////////////////////////////////////////////
RCSimGUI::~RCSimGUI() = default;

void RCSimGUI::OnStartButton()
{
  start_button_->setEnabled(false);
  stop_button_->setEnabled(true);

  pub_->Publish(gazebo::msgs::ConvertAny("start"));
}
void RCSimGUI::OnStopButton()
{
  stop_button_->setEnabled(false);
  pub_->Publish(gazebo::msgs::ConvertAny("stop"));
}
void RCSimGUI::OnShowScoreButton()
{
  // ROS_INFO_STREAM("CURRENT SCORE:" << std::endl << current_score_);
  // QMessageBox::information(this, QString("Score"), QString::fromStdString(current_score_));

  QMessageBox msg;
  // https://stackoverflow.com/questions/37668820/how-can-i-resize-qmessagebox
  auto spacer = new QSpacerItem(1200, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);
  msg.setText(QString::fromStdString(current_score_));
  auto layout = (QGridLayout*)msg.layout();
  layout->addItem(spacer, layout->rowCount(), 0, 1, layout->columnCount());
  msg.exec();
}

void RCSimGUI::CbMsg(const boost::shared_ptr<const gazebo::msgs::Any>& msg)
{
  auto text = QString::fromStdString(msg->string_value());
  text_box_->moveCursor(QTextCursor::End);
  text_box_->insertPlainText(text + "\n");
  text_box_->moveCursor(QTextCursor::End);
}

void RCSimGUI::CbTask(const boost::shared_ptr<const gazebo::msgs::Any>& msg)
{
  show_score_button_->setEnabled(true);
  current_score_ = msg->string_value();

  // todo define custom pb message so we dont have to do this and can format the display better
  auto qs = QString::fromStdString(current_score_);
  auto idx = qs.indexOf("Remaining");
  if (idx == -1)
  {
    time_label_->setText(QString("END"));
  }
  else
  {
    idx += 10;  //"remaining "
    auto end = qs.indexOf("s", idx);
    auto sub = qs.mid(idx, end - idx + 1);
    time_label_->setText(sub);
  }
}

}  // namespace gazebo

// Register this plugin with the simulator
GZ_REGISTER_GUI_PLUGIN(gazebo::RCSimGUI)