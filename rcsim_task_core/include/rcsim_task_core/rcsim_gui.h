#pragma once

#include <gazebo/common/Plugin.hh>
#include <gazebo/gui/GuiPlugin.hh>
#include <gazebo/transport/transport.hh>

namespace gazebo
{
class GAZEBO_VISIBLE RCSimGUI : public GUIPlugin
{
  Q_OBJECT

public:
  RCSimGUI();
  ~RCSimGUI() override;

  // TODO(lruegeme): replace with custom msg
  void CbTask(const boost::shared_ptr<const gazebo::msgs::Any>& msg);
  void CbMsg(const boost::shared_ptr<const gazebo::msgs::Any>& msg);
  // void resizeEvent(QResizeEvent* event) override;

protected slots:
  void OnStartButton();
  void OnStopButton();
  void OnShowScoreButton();

private:
  transport::NodePtr node_;
  transport::PublisherPtr pub_;
  transport::SubscriberPtr sub_;
  transport::SubscriberPtr sub_msg_;

  // TODO(lruegeme): replace with custom msg
  std::string current_score_;

  QLabel* time_label_;
  QPushButton* start_button_;
  QPushButton* stop_button_;
  QPushButton* show_score_button_;
  QTextEdit* text_box_;
};
}  // namespace gazebo
