#pragma once

#include <string>
#include <map>
#include <utility>

#include <ros/ros.h>
#include "boost/date_time/posix_time/posix_time.hpp"

struct ScoreItem
{
  ScoreItem() = default;

  ScoreItem(std::string desc, int max, int score = 0)
  {
    description = std::move(desc);
    max_points = max;
    points = score;
  };

  int max_points;
  int points;
  std::string description;
  // bool is_bonus = false;

  // only shown if given and excluded from max
  bool is_penalty = false;
};

class TaskScore
{
public:
  TaskScore(std::string name, const ros::Duration& duration)
  {
    name_ = std::move(name);
    max_duration_ = duration;
  }

  bool IsStarted()
  {
    return start_ != ros::Time(0);
  }

  bool IsFinished()
  {
    return duration_ != ros::Duration(0);
  }

  // add a score item to the task
  void AddItem(const std::string& key, ScoreItem item)
  {
    items_[key] = std::move(item);
  }
  // add a score item to the task
  void AddItem(const std::string& key, std::string desc, int max, int points = 0)
  {
    ScoreItem item(std::move(desc), max, points);
    items_[key] = item;
  }

  // change scoring of one item
  void AddPoints(const std::string& key, int points)
  {
    items_[key].points += points;
  }
  void SetPoints(const std::string& key, int points)
  {
    items_[key].points = points;
  }
  void SetMaxPoints(const std::string& key)
  {
    items_[key].points = items_[key].max_points;
  }

  int GetTotalScore()
  {
    int sum = 0;
    for (auto const& [key, i] : items_)
    {
      sum += i.points;
    }
    return sum;
  }

  int GetMaxScore()
  {
    int sum = 0;
    for (auto const& [key, i] : items_)
    {
      if (i.is_penalty)
      {
        continue;
      }
      sum += i.max_points;
    }
    return sum;
  }

  ros::Duration GetRemainingTime()
  {
    if (!IsStarted() || IsFinished())
    {
      return ros::Duration(0);
    }

    return start_ + max_duration_ - ros::Time::now();
  }

  void FinishTask()
  {
    if (!IsFinished() && IsStarted())
    {
      duration_ = ros::Time::now() - start_;
    }
  }

  void StartTask()
  {
    if (!IsStarted())
    {
      start_ = ros::Time::now();
      if (!IsStarted())
      {
        ROS_ERROR_STREAM("Started but Time is 0, make sure simulation is running");
      }
    }
  }

  std::string GenerateHTMLScoreString()
  {
    std::stringstream ss;
    ss.precision(2);
    ss << "<html><body>";
    ss << "<h1> Task: " << name_ << " </h1>";
    ss << "Started: " << boost::posix_time::to_simple_string(start_.toBoost()) << "<br>";
    if (duration_ == ros::Duration(0))
    {
      ss << "Remaining: " << static_cast<int>(GetRemainingTime().toSec()) << "s<br>";
    }
    else
    {
      ss << "Duration: " << static_cast<int>(duration_.toSec()) << "s / " << static_cast<int>(max_duration_.toSec())
         << "s<br>";
    }
    ss << "<table><thead><tr><th> SCORE </th><th> MAX </th><th> DESCRIPTION </th></tr></thead><tbody>";
    for (auto const& [key, i] : items_)
    {
      if (i.is_penalty)
      {
        if (i.points == 0)
        {
          continue;
        }
      }
      ss << "<tr><td>" << i.points << "</td><td>" << i.max_points << "</td><td>" << i.description << "</td></tr>";
    }
    ss << "</tbody></table><hr>";
    ss << "<b>Total: " << GetTotalScore() << " / " << GetMaxScore() << "</b>";
    ss << "</body></html>";
    return ss.str();
  }

  std::string GenerateScoreString()
  {
    std::stringstream ss;
    ss.precision(2);
    ss << "## Task: " << name_ << " ##" << std::endl;
    ss << "Started: " << boost::posix_time::to_simple_string(start_.toBoost()) << std::endl;
    if (duration_ == ros::Duration(0))
    {
      ss << "Remaining: " << static_cast<int>(GetRemainingTime().toSec()) << "s" << std::endl;
    }
    else
    {
      ss << "Duration: " << static_cast<int>(duration_.toSec()) << "s / " << static_cast<int>(max_duration_.toSec())
         << "s" << std::endl;
    }
    for (auto const& [key, i] : items_)
    {
      if (i.is_penalty)
      {
        if (i.points == 0)
        {
          continue;
        }
      }
      ss << " " << i.points << " / " << i.max_points << "\t: " << i.description << std::endl;
    }
    ss << "---" << std::endl;
    ss << "Total: " << GetTotalScore() << " / " << GetMaxScore() << std::endl;

    return ss.str();
  }

private:
  std::string name_;
  ros::Time start_{ 0 };
  ros::Duration max_duration_{ 0 };
  ros::Duration duration_{ 0 };
  std::map<std::string, ScoreItem> items_;
};